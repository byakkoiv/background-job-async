class ProductsController < ApplicationController
  before_action :set_product, only: [:show, :edit, :update, :destroy]
  
  def index
    respond_to do |format|
      format.html do  
        @products = Product.all.page(params[:page]).per(100)
      end
      format.json
      format.xlsx do
        @products = Product.all
        @job = AsyncJob.create({title: "Exporting csv", progress: 0, total: @products.count})
        exporter = Exporter.new
        exporter.file = Pathname.new('public/system/export.csv').open
        exporter.save
        ProductsExcporterJob.perform_async(@job.id, @products, exporter)
        redirect_to exporter_path(@job)
      end
    end
  end

  def show
  end
  
  def new
    @product = Product.new
  end

  def edit
  end

  def create
    @product = Product.new(product_params)

    respond_to do |format|
      if @product.save
        format.html { redirect_to @product, notice: 'Product was successfully created.' }
        format.json { render :show, status: :created, location: @product }
      else
        format.html { render :new }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @product.update(product_params)
        format.html { redirect_to @product, notice: 'Product was successfully updated.' }
        format.json { render :show, status: :ok, location: @product }
      else
        format.html { render :edit }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @product.destroy
    respond_to do |format|
      format.html { redirect_to products_url, notice: 'Product was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_product
      @product = Product.find(params[:id])
    end

    def product_params
      params.require(:product).permit(:name, :description, :price, :quantity, :rate)
    end
end
