json.extract! product, :id, :name, :description, :price, :quantity, :rate, :created_at, :updated_at
json.url product_url(product, format: :json)