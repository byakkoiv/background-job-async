class ExportMailer < ApplicationMailer
  default from: "mhernandez.tics@gmail.com"
  def send
    @products = Product.all
    xlsx = render_to_string layout: false, handlers: [:axlsx], formats: [:xlsx], template: "products /export", locals: {products: products}
    attachment = Base64.encode64(xlsx)
    attachments["Users.xlsx"] = {mime_type: Mime::XLSX, content: attachment, encoding: 'base64'}
    mail(to: 'testing@mail.com', subject: 'testing mail sending')
  end
end
