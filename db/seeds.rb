# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
#
4000.times do
  Product.create(name: Faker::Commerce.product_name, description: Faker::Commerce.department(5), price: Faker::Commerce.price, quantity: 23, rate: 5)
end
