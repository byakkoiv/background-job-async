require 'csv'

class ProductsExcporterJob
  include SuckerPunch::Job

  def perform(job_id, products, exporter)
    job = AsyncJob.find(job_id)

    progress = job.progress
    csv_string = CSV.generate do |csv|
      csv << ['Id', 'Name', 'Description', 'Price', 'Quantity']

      products.each do |product|
        progress = progress + 1
        csv << [
          product.id,
          product.name,
          product.description,
          product.price,
          product.quantity
        ]
        job.update({progress: progress})
      end
    end
    File.open('public/system/export.csv', 'w'){ |f| f.write(csv_string)}
    exporter.file = Pathname('public/system/export.csv').open
    exporter.save

    File.open('public/system/export.csv', 'w'){ |f| f.write('')}
    job.destroy
  end
end
