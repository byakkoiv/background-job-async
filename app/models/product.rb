class Product < ActiveRecord::Base
  def percentage
    "#{self.rate * self.quantity / 100}%"
  end

  def total
    self.quantity + self.price
  end

  def otro
    self.total * self.rate
  end
end
