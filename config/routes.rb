Rails.application.routes.draw do
  resources :products do
    collection do
      get :export
    end
  end
  
  resources :exporters, only: [:show]
  namespace :api do
    namespace :v1 do
      resources :async_jobs, only: [:show]
    end
  end
  root 'products#index'
end
